export default interface IFormInput {
  email: String;
  password: String;
  passwordConfirmation: String;
}
