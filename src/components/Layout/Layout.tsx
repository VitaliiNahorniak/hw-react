import { NavLink, Outlet } from "react-router-dom";
import "./Layout.scss";

const Layout: React.FC = () => {
  return (
    <>
      <header className="App-header">
        <NavLink to="/sign-in">Sign in</NavLink>
        <NavLink to="/sign-up">Registration</NavLink>
        <NavLink to="/reset-password">Reset</NavLink>
        <NavLink to="/forgot-password">Forgot</NavLink>
      </header>

      <main className="App-main">
        <div className="App-main__inner">
          <Outlet />
        </div>
      </main>
    </>
  );
};

export default Layout;
