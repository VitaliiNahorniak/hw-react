import { DetailedHTMLProps } from "react";
import "./FormHeader.scss";
import React from "react";

const FormHeader = (
  props: DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > & { message: string; logoTitle: string }
) => {
  return (
    <div className="form-header">
      <div className="form-header__logo">
        <img src={require("./img/logo.png")} alt="logo" />
      </div>
      <p className="form-header__mess">{props.message}</p>
      {props.logoTitle && (
        <div>
          <p className="form-header__title">{props.logoTitle}</p>
        </div>
      )}
    </div>
  );
};

export default FormHeader;
