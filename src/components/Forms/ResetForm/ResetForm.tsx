import { useForm, SubmitHandler } from "react-hook-form";
import "../Forms.scss";
import "./ResetForm.scss";
import IFormInput from "../../../interfaces/IFormInput";
import FormHeader from "../FormHeader/FormHeader";
import { useNavigate } from "react-router-dom";

const SignInForm = () => {
  const navigate = useNavigate();

  const goBack = () => navigate(-1);

  const { register, handleSubmit, formState, reset } = useForm<IFormInput>({
    mode: "onBlur",
  });

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    alert(JSON.stringify(data));
    reset();
  };

  const checkRules = {
    required: {
      value: true,
      message: "Is required",
    },
    minLength: {
      value: 6,
      message: "Minimum 6 characters",
    },
  };

  return (
    <div className="form">
      <FormHeader
        message={"Reset Password"}
        logoTitle={
          "Don't worry, happens to the best of us. Enter the email address associated with your account and we'll send you a link to reset."
        }
      />

      <form
        className="form__inputs"
        // noValidate
        autoComplete="on"
        onSubmit={handleSubmit(onSubmit)}
      >
        <label>
          <p className="input-lable">Email</p>
          <input type="email" {...register("email", checkRules)} />
          {formState.errors?.email && (
            <p className="input-error">
              {formState.errors?.email.message || "Error!!!"}
            </p>
          )}
        </label>

        <input type="submit" value={"Reset"} disabled={!formState.isValid} />
      </form>

      <p className="button-reset" onClick={goBack}>
        Cancel
      </p>
    </div>
  );
};

export default SignInForm;
