import { useForm, SubmitHandler } from "react-hook-form";
import "../Forms.scss";
import "./ForgotForm.scss";
import IFormInput from "../../../interfaces/IFormInput";
import { useState } from "react";
import FormHeader from "../FormHeader/FormHeader";

const ForgotForm = () => {
  const { register, handleSubmit, formState, reset } = useForm<IFormInput>({
    mode: "onBlur",
  });

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    alert(JSON.stringify(data));
    reset();
  };

  const [passwordShown, setPasswordShown] = useState(false);

  const togglePassword = () => {
    setPasswordShown((passwordShown) => !passwordShown);
  };

  const checkRules = {
    required: {
      value: true,
      message: "Is required",
    },
    minLength: {
      value: 6,
      message: "Minimum 6 characters",
    },
  };

  return (
    <div className="form">
      <FormHeader message={"Reset your password"} logoTitle={""} />

      <form
        className="form__inputs"
        // noValidate
        autoComplete="on"
        onSubmit={handleSubmit(onSubmit)}
      >
        <label>
          <p className="input-lable">New password</p>
          <input
            type={passwordShown ? "text" : "password"}
            id="show"
            {...register("password", checkRules)}
          />
          {formState.errors?.password && (
            <p className="input-error">
              {formState.errors?.password.message || "Error!!!"}
            </p>
          )}
        </label>

        <label>
          <p className="input-lable">Confirm password</p>
          <input
            type={passwordShown ? "text" : "password"}
            {...register("passwordConfirmation", checkRules)}
          />
          {formState.errors?.passwordConfirmation && (
            <p className="input-error">
              {formState.errors?.passwordConfirmation.message || "Error!!!"}
            </p>
          )}
        </label>

        <label className="checkbox">
          <input
            id="checkbox"
            type="checkbox"
            checked={passwordShown}
            onChange={togglePassword}
          />
          <p className="checkbox__lable">Show password</p>
        </label>

        <input type="submit" value={"Reset"} disabled={!formState.isValid} />
      </form>
    </div>
  );
};

export default ForgotForm;
