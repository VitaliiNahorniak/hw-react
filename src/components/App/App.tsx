import React from "react";
import "./App.scss";
import { Route, Routes } from "react-router-dom";
import Notfoundpage from "../../pages/Notfoundpage";
import Layout from "../Layout/Layout";
import SignUpPage from "../../pages/SignUpPage";
import SignInPage from "../../pages/SignInPage";
import ResetPage from "../../pages/ResetPage";
import ForgotPage from "../../pages/ForgotPage";
import WelcomePage from "../../pages/WelcomePage";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<WelcomePage />} />

          <Route path="sign-in" element={<SignInPage />} />
          <Route path="sign-up" element={<SignUpPage />} />
          <Route path="reset-password" element={<ResetPage />} />
          <Route path="forgot-password" element={<ForgotPage />} />

          <Route path="*" element={<Notfoundpage />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
