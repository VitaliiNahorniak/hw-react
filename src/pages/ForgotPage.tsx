import ForgotForm from "../components/Forms/ForgotForm/ForgotForm";

const ForgotPage: React.FC = () => {
  return (
    <div>
      <ForgotForm />
    </div>
  );
};

export default ForgotPage;
