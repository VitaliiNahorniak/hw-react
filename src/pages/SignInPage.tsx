import SignInForm from "../components/Forms/SignInForm/SignInForm";

const SignInPage: React.FC = () => {
  return (
    <div>
      <SignInForm />
    </div>
  );
};

export default SignInPage;
