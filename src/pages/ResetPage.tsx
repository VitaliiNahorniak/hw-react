import ResetForm from "../components/Forms/ResetForm/ResetForm";

const ResetPage: React.FC = () => {
  return (
    <div>
      <ResetForm />
    </div>
  );
};

export default ResetPage;
