import { Link } from "react-router-dom";
import FormHeader from "../components/Forms/FormHeader/FormHeader";

const Notfoundpage: React.FC = () => {
  return (
    <div>
      <FormHeader message={"Go home"} logoTitle={""} />
      Page doesn't exist. You can come back <Link to="/sign-in">home</Link>
    </div>
  );
};

export default Notfoundpage;
