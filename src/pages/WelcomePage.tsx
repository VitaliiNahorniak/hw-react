import FormHeader from "../components/Forms/FormHeader/FormHeader";

const WelcomePage: React.FC = () => {
  return (
    <div>
      <FormHeader message={"Welcome!"} logoTitle={""} />
    </div>
  );
};

export default WelcomePage;
